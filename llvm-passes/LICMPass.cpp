/*
 * Dummy loop pass to serve as a starting point for your Assignment 2 passes.
 * It goes through every basicblock in each loop and checks that every block
 * inside the loop is dominated by the header (which should always be the case).
 * For an example of a function pass, see DummyPass.cpp.
 */

#define DEBUG_TYPE "LICMPass"
#include "utils.h"

namespace {
    class LICMPass : public LoopPass {
    public:
        static char ID;
        LICMPass() : LoopPass(ID) {}
        virtual bool runOnLoop(Loop *L, LPPassManager &LPM) override;
        void getAnalysisUsage(AnalysisUsage &AU) const override;
    };
}

void LICMPass::getAnalysisUsage(AnalysisUsage &AU) const {
    // Tell LLVM we need some analysis info which we use for analyzing the
    // DominatorTree.
    AU.setPreservesCFG();
    AU.addRequired<LoopInfoWrapperPass>();
    getLoopAnalysisUsage(AU);
}

bool LICMPass::runOnLoop(Loop *L, LPPassManager &LPM) {
    bool changed = false;
    BasicBlock *Preheader = L->getLoopPreheader();
    BasicBlock *Header = L->getHeader();
    DominatorTree *DT = &getAnalysis<DominatorTreeWrapperPass>().getDomTree();
    std::vector<Loop *> subLoops = L->getSubLoops();
    SmallVector<Instruction *, 32> toHoistInst;
    SmallVector<BasicBlock *, 4> exitBlocks;
    L->getExitBlocks(exitBlocks);

    bool isInSubLoop = false;
    bool isExitDominant = true;
    for (BasicBlock *BB : L->blocks()) {
        if (DT->dominates(Header, BB)) {
            for(Loop *SL : subLoops) {
                if(SL->contains(BB)) {
                    isInSubLoop = true;
                    break;
                }
            }
            for(BasicBlock *EB : exitBlocks) {
                if(!DT->dominates(BB, EB)) {
                    isInSubLoop = false;
                    break;
                }
            }
            if(!isInSubLoop && isExitDominant){
                for (BasicBlock::iterator DI = BB->begin(); DI != BB->end();) {
                    Instruction *I = &*DI++;
                    if( I->isBinaryOp() || I->isShift() || isa<SelectInst>(I) || I->isCast() || isa<GetElementPtrInst>(I)){
                        DEBUG_LINE("found an instruction in " << I->getOpcodeName());
                        if(!I->mayHaveSideEffects() && L->hasLoopInvariantOperands(I)) {
                            toHoistInst.push_back(I);
                            DEBUG_LINE("adding for hoist " << I->getOpcodeName());
                        }
                    }
                }
            }
            isInSubLoop = false;
            isExitDominant = true;
        }
    }

    for(Instruction *I : toHoistInst) {
        I->moveBefore(Preheader->getTerminator());
        changed = true;
    }

    return changed;
}

char LICMPass::ID = 0;
RegisterPass<LICMPass> X("coco-licm", "CoCo Dominator Tree Example");
