/*
 * Dummy (and minimal) function pass to serve as a starting point for your
 * Assignment 2 passes. It simply visits every function and prints every call
 * instruction it finds.
 */

#define DEBUG_TYPE "ADCEPass"
#include "utils.h"
//#include <llvm/ADT/SmallVector.h>

namespace {
    class ADCEPass : public FunctionPass {
    public:
        static char ID;
        ADCEPass() : FunctionPass(ID) {}
        virtual bool runOnFunction(Function &F) override;
    };
}

bool ADCEPass::runOnFunction(Function &F) {
    DEBUG_LINE("Visiting function " << F.getName());
    bool changed = false;
    SmallVector<Instruction *, 32> WorkList;
    SmallVector<Instruction *, 32> DeadList;
    SmallSet<Instruction *, 32> LiveSet;
    
    for (df_iterator<BasicBlock*> DI = df_begin(&F.getEntryBlock()),
         DE = df_end(&F.getEntryBlock()); DI != DE; ++DI) {
        BasicBlock *BB = *DI;
        for (BasicBlock::iterator DI = BB->begin(); DI != BB->end();) {
            Instruction *I = &*DI++;
            if (I->mayHaveSideEffects() || isa<TerminatorInst>(I) || isa<StoreInst>(I) || isa<CallInst>(I)) { // OR is volatile load
                WorkList.push_back(I);
                LiveSet.insert(I); //MarkLive(I)
            } else if(LoadInst *LI = dyn_cast<LoadInst>(I)) {
                if(LI->isVolatile()) {
                    WorkList.push_back(I);
                    LiveSet.insert(I); //MarkLive(I)
                } else if (I->use_empty()) {
                    DeadList.push_back(I);
                }
            } else if (I->use_empty()) {
                DeadList.push_back(I); //Stage I for removal
                changed = true;
            }
        }
    }
    
    for (Instruction *I : DeadList) { //Remove dead instructions
        I->eraseFromParent();
    }

    while(!WorkList.empty()) {
        Instruction *I = WorkList.pop_back_val();
        for (Use &OI : I->operands()) {
            if (Instruction *Inst = dyn_cast<Instruction>(OI)) {
                LiveSet.insert(Inst);
            }
        }
    }
    for (df_iterator<BasicBlock*> DI = df_begin(&F.getEntryBlock()),
         DE = df_end(&F.getEntryBlock()); DI != DE; ++DI) {
        BasicBlock *BB = *DI;
        for (BasicBlock::iterator DI = BB->begin(); DI != BB->end();) {
            Instruction *I = &*DI++;
            if (!LiveSet.count(I) && I->use_empty()) { // OR is call
                I->dropAllReferences();
            }
        }
    }
    for (df_iterator<BasicBlock*> DI = df_begin(&F.getEntryBlock()),
         DE = df_end(&F.getEntryBlock()); DI != DE; ++DI) {
        BasicBlock *BB = *DI;
        for (BasicBlock::iterator DI = BB->begin(); DI != BB->end();) {
            Instruction *I = &*DI++;
            if (!LiveSet.count(I) && I->use_empty()) { // OR is call
                I->eraseFromParent();
                changed = true;
            }
        }
    }
    DEBUG_LINE("Finished function " << F.getName());

    return changed;  // We did alter the IR
}

// Register the pass with LLVM so we can invoke it with opt. The first argument
// to RegisterPass is the commandline switch to run this pass (e.g., opt
// -coco-adce, the second argument is a description shown in the help text
// about this pass.
char ADCEPass::ID = 0;
static RegisterPass<ADCEPass> X("coco-adce", "Example LLVM pass printing each function it visits, and every call instruction it finds");
