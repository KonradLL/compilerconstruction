/*
 * Dummy module pass which can be used as example and starting point for
 * Assignment 3. This pass inserts calls to a helper function for every stack
 * allocation. The helper function prints the size of the stack allocation and
 * serves as an example how to create and insert helpers. The implementation of
 * the helper function can be found in runtime/dummy.c.
 */

#define DEBUG_TYPE "BoundsCheckerPass"
#define INT_TYPE 11
#define ARG_SIZE_APPEND "_size_"
#define MAIN "main"
#include "utils.h"
#include <stdexcept>

namespace {
    class BoundsCheckerPass : public ModulePass {
    public:
        static char ID;
        BoundsCheckerPass() : ModulePass(ID) {}
        virtual bool runOnModule(Module &M) override;

    private:
        Function *CheckBoundsFunc;
        bool isInt(Value *V);
        Argument* getArgument(Function* F, int idx);
        Function& replaceFunction(Function& F, int Arrs, SmallVector<std::vector<Function*>, 8> *RF);
        Value* getOffsetValue(GetElementPtrInst *GEP, IRBuilder<> *B);
        Value* getSizeValue(Value *Val, LLVMContext &Context, bool* shouldReloop);
        bool handleFunction(Function &F, bool* shouldReloop, SmallVector<std::vector<Function*>, 8> *RF);
    };
}

// check if value is of Int32Type
bool BoundsCheckerPass::isInt(Value *V) {
    return V && V->getType()->isIntegerTy();
}

/*
 * Get a function argument at a specific index
 */
Argument* BoundsCheckerPass::getArgument(Function* F, int idx) {
    int i = 0;
    for (Argument& Arg : F->args()) {
        if (i++ == idx)
            return &Arg;
    }
    ERROR("Argument index out of bounds " << F->arg_size());
}

/*
 * Create a copy for function that contains arrays, and add a size argument for each array
 * the is named as '_size_'+arr_name
 */
Function& BoundsCheckerPass::replaceFunction(Function& F, int Arrs, SmallVector<std::vector<Function*>, 8> *RF) {
    if (!Arrs || F.getName().str() == MAIN)
        return F;

    SmallVector<Argument*, 4> newArgs;
    Type *Int32Ty = Type::getInt32Ty(F.getContext());
    std::vector<Type*> ArgTypes;
    for (int i = 0; i < Arrs; i++) {
        ArgTypes.push_back(Int32Ty);
    }

    Function* newFunc = addParamsToFunction(
                &F, ArgTypes, newArgs);

    int k = 0;
    for (Argument &newFuncArg : newFunc->args()) {
        if (newFuncArg.getType()->isPointerTy()) {
            newArgs[k]->setName(ARG_SIZE_APPEND + newFuncArg.getName());
            k++;
        }
    }

    RF->push_back({&F, newFunc});
    return *newFunc;
}

/*
 * Get the size for a given array by going up the instructions till a alloca or const is found
 */
Value* BoundsCheckerPass::getSizeValue(Value *Val, LLVMContext &Context, bool *reloop) {
    Value* ret = NULL;

    if (GetElementPtrInst *GEP = dyn_cast<GetElementPtrInst>(Val)) {
        DEBUG_LINE("Size GEP");
        ret = getSizeValue(GEP->getOperand(0), Context, reloop);

    } else if (AllocaInst *AI = dyn_cast<AllocaInst>(Val)) {
        DEBUG_LINE("Size alloc");
        ret = AI->getArraySize();

    } else if (LoadInst *LI = dyn_cast<LoadInst>(Val)) {
        DEBUG_LINE("Size Load"); 
        while (Instruction* prevInst = LI->getPrevNode()) {
            if (StoreInst *SI = dyn_cast<StoreInst>(prevInst))
                if(SI->getPointerOperand() == LI->getPointerOperand())
                    ret = getSizeValue(SI->getValueOperand(), Context, reloop);
                    break;
        }

    } else if (StoreInst *SI = dyn_cast<StoreInst>(Val)) { 
        DEBUG_LINE("Store inst");
        ret = getSizeValue(SI->getValueOperand(), Context, reloop);

    } else if (Argument *Ar = dyn_cast<Argument>(Val)) {
        DEBUG_LINE("Size arg");
        if (Ar->getParent()->getName().str() == MAIN
            && getArgument(Ar->getParent(), 1) == Ar
            && Ar->getType()->isPointerTy()) {
                ret = getArgument(Ar->getParent(), 0);
        } else {
            for (Argument& FuncArg : Ar->getParent()->args()) {
                if (FuncArg.getName().str() == (ARG_SIZE_APPEND + Ar->getName().str()) )
                    ret = &FuncArg;
            }
        }
        if (!ret)
            *reloop = true;

    } else if (Constant *C = dyn_cast<Constant>(Val)) { //THIS IS A MESS
        DEBUG_LINE("Size Const");
        Type* Ty = C->getType();
        PointerType* PT = cast<PointerType>(Ty);
        Type* PTy = PT->getElementType();
        ArrayType* AT = cast<ArrayType>(PTy);
        Type* IntType = Type::getInt32Ty(Context);
        Constant* CInt = ConstantInt::get(IntType, AT->getNumElements());
        ret = cast<Value>(CInt);

    } else if (PHINode *PHIN = dyn_cast<PHINode>(Val)) {
        DEBUG_LINE("Size Phi");
        Type* IntType = Type::getInt32Ty(Context);
        unsigned PHINBlocks = PHIN->getNumIncomingValues(); 
        PHINode* PHISize = PHINode::Create(IntType, PHINBlocks);

        for(unsigned i = 0; i < PHINBlocks; i++) {
            Value* IncVal;
            Value* CurrPhiVal = PHIN->getIncomingValue(i);
            if (CurrPhiVal->getName() == PHIN->getName()){
                IncVal = PHISize;
            } else {
                IncVal = getSizeValue(PHIN->getIncomingValue(i), Context, reloop);
            }

            BasicBlock* IncBlock = PHIN->getIncomingBlock(i);
            PHISize->addIncoming(IncVal, IncBlock);
        }
        

        PHISize->setName(PHIN->getName().str() + "_size");
        PHISize->insertAfter(PHIN);
        ret = cast<Value>(PHISize);

    } else {
        ERROR("Unknown origin for GEP");
    }
    return ret;
}

/*
 * Find the offset for a given GEP
 */
Value* BoundsCheckerPass::getOffsetValue(GetElementPtrInst *GEP, IRBuilder<> *B) {
    DEBUG_LINE("getOffset");
    Value* op1 = GEP->getOperand(0);
    Value* off = GEP->getOperand(1);
    Value* ret = off;

    if(!isInt(off)) {
        ERROR("GEP offset isn't an int");
    }

    if (GetElementPtrInst *preGEP = dyn_cast<GetElementPtrInst>(op1)) {
        Value* preGEPoff = getOffsetValue(preGEP, B);
        B->SetInsertPoint(GEP);
        ret = B->CreateAdd(preGEPoff, off);
    }
    return ret;
}

/*
 * Go through the function and insert boundchecks on GEPs and replace CallInst to the new function
 */
bool BoundsCheckerPass::handleFunction(Function &F, bool* shouldReloop, SmallVector<std::vector<Function*>, 8> *RF) {
    bool Changed = false;
    
    IRBuilder<> B(&F.getEntryBlock());
    std::vector<std::vector<CallInst*>> ToReplace;

    for (Instruction &II : instructions(F)) {
        Instruction *I = &II;
        DEBUG_LINE("In: " << II.getOpcodeName());
        if (GetElementPtrInst *GEP = dyn_cast<GetElementPtrInst>(I)) {

            DEBUG_LINE("GEP instruction");
            Value* Voffset = getOffsetValue(GEP, &B);
            Value* Vsize = getSizeValue(cast<Value>(GEP), F.getContext(), shouldReloop);

            if(!isInt(Voffset)) {
                DEBUG_LINE("offset is not int32");
                continue;
            }
            if(!isInt(Vsize)) {
                DEBUG_LINE("Size is not int32");
                continue;
            }

            DEBUG_LINE("off " << Voffset->getValueID());
            DEBUG_LINE("size " << Vsize->getValueID());
            
            B.SetInsertPoint(GEP);
            B.CreateCall(CheckBoundsFunc, { Voffset, Vsize });
            Changed = true;

        } else if (CallInst *CI = dyn_cast<CallInst>(I)) {
            DEBUG_LINE("Call inst " << CI->getCalledFunction()->getName());
            std::vector<Function*> RPFuncts;
            for (std::vector<Function*> Functs : *RF) {
                if (Functs[0]->getName() == CI->getCalledFunction()->getName()) {
                    RPFuncts = std::move(Functs);
                }
            }
            if (RPFuncts.empty())
                continue;
            
            std::vector<Value*> args;
            for(Value* arg : CI->arg_operands()) {
                args.push_back(arg);
            }
            for(Value* Arg : CI->arg_operands()) {
                if (isa<PointerType>(Arg->getType())) {
                    Value* size_arg = getSizeValue(Arg, F.getContext(), shouldReloop);
                    if (!size_arg)
                        return Changed;
                    args.push_back(size_arg);
                }
            }

            CallInst* callTemp = CallInst::Create(RPFuncts[1], args);
            ToReplace.push_back({CI, callTemp});
            Changed = true;

        }
    }

    for (std::vector<CallInst*> Replacing : ToReplace) {
        llvm::ReplaceInstWithInst(Replacing[0], Replacing[1]);
    }
    return Changed;
}

bool BoundsCheckerPass::runOnModule(Module &M) {
    // Retrieve a pointer to the helper function. The handleFunction
    // function will insert calls to this function for every allocation. This
    // function is written in our runtime (runtime/dummy.c). To see its (LLVM)
    // type, you can check runtime/obj/dummy.ll)
    LLVMContext &C = M.getContext();
    Type *VoidTy = Type::getVoidTy(C);
    Type *Int32Ty = Type::getInt32Ty(C);

    //  void @__coco_check_bounds(i32 %offset, i32 %array_size)
    CheckBoundsFunc =
        cast<Function>(M.getOrInsertFunction("__coco_check_bounds",
                    VoidTy, Int32Ty, Int32Ty));

    bool Changed = false;

    SmallPtrSet<Function*, 8> WorkList;
    SmallVector<Function*, 8> WorkListVec;
    SmallVector<std::vector<Function*>, 8> ReplacedFunct;
    for (Function &F : M) {
        int k = 0;
        for (Value& FuncArg : F.args()) {
            if (isa<PointerType>(FuncArg.getType()))
                k++;
        }

        if (shouldInstrument(&F)){
            Function& theFunc = replaceFunction(F, k, &ReplacedFunct);
            WorkList.insert(&theFunc);
            WorkListVec.push_back(&theFunc);
        }
    }

    
    while(!WorkList.empty()) {

        SmallVector<Function*, 8> NewWorkListVec;
        for (Function* F : WorkListVec) {
            // We want to skip instrumenting certain functions, like declarations
            // and helper functions (e.g., our dummy_print_allocation)
            DEBUG_LINE("Visiting function " << F->getName());
            if (!shouldInstrument(F))
                continue;

            bool shouldReloop = false;
            SmallVector<std::vector<Function*>, 8> ReplaceFunct;
            Changed |= handleFunction(*F, &shouldReloop, &ReplacedFunct);
            if (shouldReloop){
                NewWorkListVec.push_back(F);
            } else {
                WorkList.erase(F);
            }
        }
        WorkListVec = std::move(NewWorkListVec);
    }

    
    while (!ReplacedFunct.empty()) {
        SmallVector<std::vector<Function*>, 8> NewReplacedFunct;
        for (std::vector<Function*> Functs : ReplacedFunct) {
            if (Functs[0]->use_empty()) {
                Functs[0]->eraseFromParent();
            } else {
                NewReplacedFunct.push_back(Functs);
            }
        }
        ReplacedFunct = std::move(NewReplacedFunct);
    }
    
    return Changed;
}

// Register the pass with LLVM so we can invoke it with opt. The first argument
// to RegisterPass is the commandline switch to run this pass (e.g., opt
// -coco-boundscheck, the second argument is a description shown in the help
// text about this pass.
char BoundsCheckerPass::ID = 0;
static RegisterPass<BoundsCheckerPass> X("coco-boundscheck", "Example LLVM module pass that inserts prints for every allocation.");
