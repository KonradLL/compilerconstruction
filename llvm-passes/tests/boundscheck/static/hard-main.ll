; CHECK: @main(i32 %param.argc, i8** %argvcv)
define i32 @main(i32 %param.argc, i8** %argvcv) {
; CHECK: %a = add i32 2, 1
  %a = add i32 2, 1
; CHECK: call void @__coco_check_bounds({{.*}}i32 %a, i32 %param.argc)
; CHECK: %argv.idx = getelementptr i8*, i8** %argvcv, i32 %a
  %argv.idx = getelementptr i8*, i8** %argvcv, i32 %a
  ret i32 0
}
