; CHECK: @f
define void @f() {
; CHECK: %a = alloca i32, i32 16
  %a = alloca i32, i32 16
  %c = alloca i32*
; CHECK: call void @__coco_check_bounds({{.*}}i32 0, i32 16{{.*}})
; CHECK: %a.idx = getelementptr i32, i32* %a, i32 0
  %a.idx = getelementptr i32, i32* %a, i32 0
  store i32* %a.idx, i32** %c
; CHECK: %b = load i32*, i32** %c
  %b = load i32*, i32** %c
; CHECK: call void @__coco_check_bounds({{.*}}i32 100, i32 16{{.*}})
; CHECK: %b.idx = getelementptr i32, i32* %b, i32 100
  %b.idx = getelementptr i32, i32* %b, i32 100
  store i32 1337, i32* %b.idx
  ret void
}
