; CHECK: @g{{.*}}(i32* %a, i32* %b, i32 [[ASARG:%.*]], i32 [[BSARG:%.*]])
define void @g(i32* %a, i32* %b) {
; CHECK: call void @__coco_check_bounds({{.*}}i32 100, i32 [[BSARG]]{{.*}})
; CHECK: %b.idx = getelementptr i32, i32* %b, i32 100
  %b.idx = getelementptr i32, i32* %b, i32 100
  store i32 1337, i32* %b.idx
  ret void
}
; CHECK: @f(i32 %s)
define void @f(i32 %s) {
; CHECK: %arr = alloca i32, i32 %s
  %arr = alloca i32, i32 %s
; CHECK: %arr2 = alloca i32, i32 16
  %arr2 = alloca i32, i32 16
; CHECK: call void @g{{.*}}(i32* %arr, i32* %arr2, i32 %s, i32 16)
  call void @g(i32* %arr, i32* %arr2);
  ret void
}
