/*
 * Dummy (and minimal) function pass to serve as a starting point for your
 * Assignment 2 passes. It simply visits every function and prints every call
 * instruction it finds.
 */

#include "utils.h"
#define DEBUG_TYPE "ConstPropPass"
#define ADD 11
#define SUB 13
#define MUL 15
#define SHL 23
#define SHR 25
#define UNKNOWN 184467440737095516

namespace {
    class ConstPropPass : public FunctionPass {
    public:
        static char ID;
        ConstPropPass() : FunctionPass(ID) {}
        virtual bool runOnFunction(Function &F) override;
    private:
        uint64_t doArithmetic(unsigned op, SmallVector<ConstantInt*, 2> Constants);
        Value* foldInstruction(Instruction &I, Function &F);
    };
}

bool ConstPropPass::runOnFunction(Function &F) {
    LOG_LINE("Visiting function " << F.getName());

    bool changed = false;
    SmallPtrSet<Instruction *, 16> WorkList;
    SmallVector<Instruction *, 16> WorkListVec;
    for (Instruction &I : instructions(&F)) {
        WorkList.insert(&I);
        WorkListVec.push_back(&I);
    }

    while(!WorkList.empty()){

        SmallVector<Instruction*, 16> NewWorkListVec;
        for (Instruction *I : WorkListVec) {
            WorkList.erase(I);
 
            if (!I->use_empty()) {
                if (Value *V = foldInstruction(*I, F)) {
                    
                    for (User *U : I->users()) {
                        if (WorkList.insert(cast<Instruction>(U)).second)
                            NewWorkListVec.push_back(cast<Instruction>(U));
                    }

                    I->replaceAllUsesWith(V);
                    changed = true;
                }
            }
        }
        WorkListVec = std::move(NewWorkListVec);
    }

    return changed;
}

uint64_t ConstPropPass::doArithmetic(unsigned Op, SmallVector<ConstantInt*, 2> Constants) {
    uint64_t rhs = Constants.pop_back_val()->getZExtValue();
    uint64_t lhs = Constants.pop_back_val()->getZExtValue();
    LOG_LINE("const val " << lhs << " & " << rhs);

    uint64_t result;
    switch(Op) {
        case ADD:
            result = lhs + rhs;
            break;
        case SUB:
            result = lhs - rhs;
            break;
        case MUL:
            result = lhs * rhs;
            break;
        case SHL:
            result = lhs << rhs;
            break;
        case SHR:
            result = lhs >> rhs;
            break;
        default:
            LOG_LINE("unknown OpCode");
            result = UNKNOWN;
            break;
    }
    LOG_LINE("Result: " << result);
    
    return result;
}

Value* ConstPropPass::foldInstruction(Instruction &I, Function &F){
    LOG_LINE("");
    LOG_LINE("Instruction: " << I.getOpcodeName() << ", code: " << I.getOpcode());
    Value* V = NULL;
    SmallVector<ConstantInt *, 2> Constants;
    SmallVector<Use *, 2> Operands;

    for (Use &OI : I.operands()) {
        Operands.push_back(&OI);
        Value* val = OI.get();
        if(ConstantInt *C = dyn_cast<ConstantInt>(OI)) {
            LOG_LINE("Const value: " << C->getZExtValue());
            Constants.push_back(C);
        } else {
            LOG_LINE("Var with name: " << val->getName() << ", Value: " << val);
        }
    }

    ConstantInt *constInt;
    if(Constants.size() == 2) {
        uint64_t newValue = doArithmetic(I.getOpcode(), Constants);
        if(newValue != UNKNOWN) {
            constInt = ConstantInt::get(Constants.pop_back_val()->getType() , newValue);
            if(ConstantData *CD = dyn_cast<ConstantData>(constInt))
                if(Constant *C = dyn_cast<Constant>(CD))
                    if(User *U = dyn_cast<User>(C))
                        if(Value *nV = dyn_cast<Value>(U))
                            V = nV;
            LOG_LINE("newVal: " << constInt->getZExtValue());
        }

    } else if (Operands.size() == 2 && Constants.size() == 1) {
        Use *Urhs = Operands.pop_back_val();
        Use *Ulhs = Operands.pop_back_val();
        unsigned opc = I.getOpcode();

        uint64_t rhs;
        uint64_t lhs;
        if (ConstantInt *C = dyn_cast<ConstantInt>(Urhs))
            rhs = C->getZExtValue();
        if (ConstantInt *C = dyn_cast<ConstantInt>(Ulhs))
            lhs = C->getZExtValue();


        if(rhs == 0 && (opc == SUB || opc == SHL || opc == SHR || opc == ADD)) {
            V = Ulhs->get();
        } else if (lhs == 0 && opc == ADD) {
            V = Urhs->get();
        } else if (lhs == 1 && opc == MUL) {
            V = Urhs->get();
        } else if (rhs == 1 && opc == MUL) {
            V = Ulhs->get();
        } else if (lhs == 0 && opc == MUL) {
            V = Ulhs->get();
        } else if (rhs == 0 && opc == MUL) {
            V = Urhs->get();
        } else if (lhs == 0 && opc == SHL) {
            V = Ulhs->get();
        } else if (rhs == 0 && opc == SHL) {
            V = Ulhs->get();
        } else if (lhs == 0 && opc == SHR) {
            V = Ulhs->get();
        } else if (rhs == 0 && opc == SHR) {
            V = Ulhs->get();
        }
    }

    return V;
}

// Register the pass with LLVM so we can invoke it with opt. The first argument
// to RegisterPass is the commandline switch to run this pass (e.g., opt
// -coco-dummypass, the second argument is a description shown in the help text
// about this pass.
char ConstPropPass::ID = 0;
static RegisterPass<ConstPropPass> X("coco-constprop", "Example LLVM pass printing each function it visits, and every call instruction it finds");