from util import ASTTransformer, NodeError
from ast import Type, Operator, VarDef, ArrayDef, Assignment, Modification, \
        If, Block, VarUse, BinaryOp, IntConst, Return, While, DoWhile


class Desugarer(ASTTransformer):
    def __init__(self):
        self.varcache_stack = [{}]

    def makevar(self, name):
        # Generate a variable starting with an underscore (which is not allowed
        # in the language itself, so should be unique). To make the variable
        # unique, add a counter value if there are multiple generated variables
        # of the same name within the current scope.
        # A variable can be tagged as 'ssa' which means it is only assigned once
        # at its definition.
        name = '_' + name
        varcache = self.varcache_stack[-1]
        occurrences = varcache.setdefault(name, 0)
        varcache[name] += 1
        return name if not occurrences else name + str(occurrences + 1)

    def visitFunDef(self, node):
        self.varcache_stack.append({})
        self.visit_children(node)
        self.varcache_stack.pop()
        return

    def visitFor(self, node):
        # from: for ( assignment to expr ) body
        # to: { 
        #   assignment;
        #   while (cond[assignment.value to expr]) body, modif[assignment.startval + 1]
        # }
        self.visit_children(node)
        if(Type.get(node._type) != Type.get('int')):
            raise NodeError(node, 'Error: For cant use non-int in condition')
        
        nameRef = VarUse(node.name)
        return Block([
                VarDef(Type.get(node._type), node.name, node.startval),
                While( BinaryOp(nameRef, Operator.get('<'), node.stopval),
                 Block([
                  node.body,
                  Assignment(nameRef, BinaryOp(nameRef, Operator.get('+'), IntConst(1)))
               ]))]).at(node)

    # def visitBinaryOp(self, node):
    #     # from: (float)lhs / (float)rhs
    #     # to: (float)lhs // (float)rhs
    #     self.visit_children(node)
    #     if(Type.get(node.rhs) == Type.get('float')):
    #         return BinaryOp(node.lhs, Operator.get('//'), node.rhs)
    #     else:
    #         return node


    def visitModification(self, m):
        # from: lhs op= rhs
        # to:   lhs = lhs op rhs
        self.visit_children(m)
        return Assignment(m.ref, BinaryOp(m.ref, m.op, m.value)).at(m)
