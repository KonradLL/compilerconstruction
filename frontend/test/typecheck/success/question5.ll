; ModuleID = '<string>'
source_filename = "<string>"
target triple = "x86_64-unknown-linux-gnu"

define i32 @main(i32 %param.argc, i8** %argv) {
entry:
  %argc = alloca i32
  store i32 %param.argc, i32* %argc
  %a = alloca i32, i32 0
  %.5 = bitcast i32* %a to i8*
  %nbytes = mul i32 0, 4
  call void @llvm.memset.p0i8.i32(i8* %.5, i8 0, i32 %nbytes, i32 4, i1 false)
  %a.idx = getelementptr i32, i32* %a, i32 0
  store i32 1, i32* %a.idx
  ret i32 0
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i32, i1) #0

attributes #0 = { argmemonly nounwind }
