/*
 * Check if the requested array slot is within the bounds of the array,
 * if it is not in slot an error is raised
 */
#include <stdio.h>
#include <stdlib.h>

void __coco_check_bounds(int offset, int array_size) {
    if (offset < 0 || offset >= array_size) {
        fprintf( stderr, "my %s has %d chars\n", "string format", 30);
        exit(1);
    }
}
